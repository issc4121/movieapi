from schemas.user import User
from fastapi import APIRouter
from fastapi.responses import JSONResponse
from utils.jwt_manager import create_token

user_router = APIRouter()

@user_router.post('/login', tags = ['auth'])
def login(user: User):
    if user.email == "victor7044@gmail.com" and user.password == "password":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
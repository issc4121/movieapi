from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(default = "Titulo de la pelicula", min_length = 1, max_length=100)
    overview: str = Field(default="Descripción de la pelicula", min_length = 1, max_length=500)
    year: int = Field(..., le=2024)
    rating: float = Field(..., ge=1, le=10)
    category: str = Field(default = "Categoria", min_length = 3, max_length=20)

    class Config:
        json_schema_extra = {
            "example": {
                "id" : 1,
                "title" : "Titulo",
                "overview":"Descripcion",
                "year" : 2004,
                "rating" : 7.8,
                "category" : "Categoria"
            }
        }
